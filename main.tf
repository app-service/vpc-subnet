module "vpc" {
  
  
  source = "git::https://gitlab.com/modules4/aws-vpc.git"
  
  vpc_name  = "Import-vpc"
  
}

module "public_subnet" {
  source = "git::https://gitlab.com/modules4/aws-public-subnet.git"
  public_subnet_name = "Pomelo public subnet"
  azs                = ["us-east-1a"]
  public_subnets     = ["10.0.101.0/24"]
}

module "private_subnet"
  source = "git::https://gitlab.com/modules4/aws-private-subnet.git"
  private_subnet_name = "Pomelo private subnet"
  azs                = ["us-east-1a"]
  public_subnets     = ["10.0.1.0/24"]
