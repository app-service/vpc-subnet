terraform {
  backend "s3" {
   
    bucket         = "terraformtfstatebackuplog"
    key            = "global/s3/demovpc/terraform.tfstate"
    region         = "us-east-1"
   
    dynamodb_table = "terraformtfstatebackupfile"
    encrypt        = true
    #role_arn     = "arn:aws:iam::310159601964:role/Terraform"
    access_key = ""
    secret_key = ""
   
 }
}

